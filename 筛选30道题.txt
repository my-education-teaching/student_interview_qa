## 1.题

题目：给你一个长度为 `n` 的整数数组，每次操作将会使 `n - 1` 个元素增加 `1` 。返回让数组所有元素相等的最小操作次数。

实列：

```
输入：nums = [1,2,3]
输出：3
解释：
只需要3次操作（注意每次操作会增加两个元素的值）：
[1,2,3]  =>  [2,3,3]  =>  [3,4,3]  =>  [4,4,4]
```

答案：

```
 var list = [1, 2, 3]

    var max = Math.max(...list)
    var count = 1

    function run() {
      for (var i in list) {
        if (list[i] < max) {
          list.forEach((v, i, o) => {
            if (v !== max) {
              o[i]++
            }
          })
          count++;
          run()
        }
      }
      return [count, list]
    }

    console.log(run())
```









## 2.题

题目：

给你一个整数数组 nums 。如果一组数字 (i,j) 满足 nums[i] == nums[j] 且 i < j ，就可以认为这是一组 好数对 。返回好数对的数目。

实列：

```
输入：nums = [1,2,3,1,1,3]
输出：4
解释：有 4 组好数对，分别是 (0,3), (0,4), (3,4), (2,5) ，下标从 0 开始
```

答案：

```
   var nums = [1, 2, 3, 1, 1, 3]
    var res = 0;
    for (var i = 0; i < nums.length; i++) {
      for (var j = 0; j < nums.length; j++) {
        if (nums[i] === nums[j] && i < j) {
          res++
        }
      }
    }
    console.log(res)
```









## 3.题

题目：给定一个正整数 n ，你可以做如下操作：

?            如果 n 是偶数，则用 n / 2替换 n 。
?            如果 n 是奇数，则可以用 n + 1或n - 1替换 n 。
?            n 变为 1 所需的最小替换次数是多少？

答案：

```
var num = 5, i = 0;
    function run(n) {
      if (n === 1) return 0
      // 是偶数
      if (n % 2 === 0) {
        num = n / 2
      } else
        // 是奇数
        if (n % 2 !== 0) {
          num = n - 1
        }
      i++
      run(num)
    }
    run(num)
    console.log(i, num)
```









## 4.题

题目：

给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。

实例：

```
输入: nums = [1,3,5,6], target = 5
输出: 2
```

答案：

```
 var list = [1, 2, 4, 5, 6]
    var target = 5
    for (var i = 0; i < list.length; i++) {
      if (list[i] === target) {
        console.log(i)
        break
      } else if (list[i + 1] - target === 1) {
        console.log(i + 1)
        break;
      } else if (list[i] > target) {
        console.log(0)
        break;
      } else if (!list[i + 1] && target > list[list.length - 1]) {
        console.log(i + 1)
        break;
      }
    }
```




## 7.题

  题目：

给你一个有序数组 `nums` ，请你原地删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。

提醒：要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。

答案：

```
  var nums = [1, 2, 3, 3, 4]
    var len = nums.length;
    function run() {
      if (len == 0) {
        return 0;
      }
      var slow = 1;
      var fast = 1;
      while (fast < len) {
        if (nums[fast] != nums[fast - 1]) {
          nums[slow] = nums[fast];
          ++slow;
        }
        ++fast;
      }
      return slow;
    }
    console.log(run())

```






### 9．一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法（先后次序不同算不同的结果）。

对于本题,前提只有 一次 1阶或者2阶的跳法。
a.如果两种跳法，1阶或者2阶，那么假定第一次跳的是一阶，那么剩下的是n-1个台阶，跳法是f(n-1);
b.假定第一次跳的是2阶，那么剩下的是n-2个台阶，跳法是f(n-2)
c.由a\b假设可以得出总跳法为: f(n) = f(n-1) + f(n-2) 
d.然后通过实际的情况可以得出：只有一阶的时候 f(1) = 1 ,只有两阶的时候可以有 f(2) = 2
e.可以发现最终得出的是一个斐波那契数列：
        
       | 1, (n=1)
f(n) = | 2, (n=2)
       | f(n-1)+f(n-2) ,(n>2,n为整数)


```
function jumpFloor2(n) {
        var target = 0, number1 = 1, number2 = 2;
 
        if(n<=0)return 0;
        if(n == 1) return 1;
        if(n==2) return 2;
        for(var i=3;i<=n;++i) {
            target = number1 + number2;
            number1 = number2;
            number2 = target;
        }
        return target;
    }
 
    console.log(jumpFloor2(100)) // 输出 573147844013817200000
```










### 10.输入一个链表，反转链表后，输出新链表的表头。


这一题还是蛮难的。链表的翻转，例如 1->2->3->4->5  ==>  5->4->3->2->1
tmp = pHead.next   把当前节点的下一个节点保存下来
pHead.next = pre   把前一个节点移到当前节点的下一个节点，因为要翻转节点，pre始终指向要反转节点的首节点
pre = pHead        当前节点向后移一位
pHead = tmp        把之前保存的下一个节点指针再给当前节点接着翻转

分析：

方法（1）：更改next指针域法。当没有节点或者只有一个结点时，直接返回这个节点；否则至少有两个节点时，用三个指针n1,n2,n3,n1指向第一个节点，n2指向第二个节点，n3指向第三节点(可能为空)，每次修改n2的next让其指向n1,然后利用n3保存下一个节点，然后利用n2移动n1和n2的相对位置（需要注意的是，最开始要将n1的next域置空，因为它反转后要做尾节点）。

```
//代码
public class Solution {
        public ListNode ReverseList(ListNode head) {

            ListNode qHead=null;//翻转后的头结点

            if(head==null || head.next == null){
                return head;
            }

            ListNode p = head;


            while (p!=null){
                ListNode nextNode = p.next;//暂存
                p.next = qHead;
                qHead = p;
                p = nextNode;
            }
            return qHead;
        }
    }

```











###11. 爬楼梯

假设你正在爬楼梯。需要 n 阶你才能到达楼顶。

每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？

注意：给定 n 是一个正整数。

示例 1：

输入： 2
输出： 2

```
 public int climbStairs(int n) {
        if(n < 3){
            return n;
        }
        int dp_i_1 = 2;
        int dp_i_2 = 1;
        int dp = 0;
        for(int i=3;i<=n;i++){
            dp = dp_i_1 + dp_i_2;
            dp_i_2 = dp_i_1;
            dp_i_1 = dp;
        }
        return dp;
    }
```














###12.打家劫舍

你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。

给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。

示例 1：

输入：[1,2,3,1]
输出：4

```
 // dp[i] = Math.max(dp[i-1],dp[i-2] + nums[i]);
    public int rob(int[] nums) {
        if(nums == null || nums.length == 0){
            return 0;
        }

        int[] dp = new int[nums.length+2];
        dp[0] = 0;
        dp[1] = 0;
        for(int i=2;i<nums.length +2;i++){
            dp[i] = Math.max(dp[i-1],dp[i-2] + nums[i-2]);
        }
        return Math.max(dp[nums.length+1],dp[nums.length]);
    }

```












### 13.回文数

```java
 public boolean isPalindrome(int x) {
     if (x < 0) {
         return false;
     }
     int help = 1;
     int tmp = x;
     while (tmp >= 10) {
         help *= 10;
         tmp /= 10;
     }
     while (x != 0) {
         if (x % 10 != x / help) {
             return false;
         }
         x = x % help / 10;
         help /= 100;
     }
     return true;
 }











### 14.猴子分桃

海滩上有一堆桃子，五只猴子来分。第一只猴子把这堆桃子凭据分为五份，多了一个，这只猴子把多的一个扔入海中，拿走了一份。第二只猴子把剩下的桃子又平均分成五份，又多了一个，它同样把多的一个扔入海中，拿走了一份，第三、第四、第五只猴子都是这样做的，问海滩上原来最少有多少个桃子？


```java
public class Test2{
	public static void main(String[] args){
		int i=0,x=0;
	while(x<5){
	i++;
	int q=i;
	for(int y=0;y<5;y++){
	if(--q%5==0){
	x++;
    q=q*4/5;
        }
	} 
		if(x==5){
		System.out.println(i);
		}else{x=0;}
	}}
}
```











### 15.快速排序

快速排序是由东尼·霍尔所发展的一种排序算法。在平均状况下，排序 n 个项目要 Ο(nlogn) 次比较。在最坏状况下则需要 Ο(n2) 次比较，但这种状况并不常见。事实上，快速排序通常明显比其他 Ο(nlogn) 算法更快，因为它的内部循环（inner loop）可以在大部分的架构上很有效率地被实现出来。

快速排序使用分治法（Divide and conquer）策略来把一个串行（list）分为两个子串行（sub-lists）。

快速排序又是一种分而治之思想在排序算法上的典型应用。本质上来看，快速排序应该算是在冒泡排序基础上的递归分治法。

快速排序的名字起的是简单粗暴，因为一听到这个名字你就知道它存在的意义，就是快，而且效率高！它是处理大数据最快的排序算法之一了。虽然 Worst Case 的时间复杂度达到了 O(n2)，但是人家就是优秀，在大多数情况下都比平均时间复杂度为 O(n logn) 的排序算法表现要更好，可是这是为什么呢，我也不知道。好在我的强迫症又犯了，查了 N 多资料终于在《算法艺术与信息学竞赛》上找到了满意的答案：

> *快速排序的最坏运行情况是 O(n2)，比如说顺序数列的快排。但它的平摊期望时间是 O(nlogn)，且 O(nlogn) 记号中隐含的常数因子很小，比复杂度稳定等于 O(nlogn) 的归并排序要小很多。所以，对绝大多数顺序性较弱的随机数列而言，快速排序总是优于归并排序。*

```java
public class QuickSort implements IArraySort {

    @Override
    public int[] sort(int[] sourceArray) throws Exception {
        // 对 arr 进行拷贝，不改变参数内容
        int[] arr = Arrays.copyOf(sourceArray, sourceArray.length);

        return quickSort(arr, 0, arr.length - 1);
    }

    private int[] quickSort(int[] arr, int left, int right) {
        if (left < right) {
            int partitionIndex = partition(arr, left, right);
            quickSort(arr, left, partitionIndex - 1);
            quickSort(arr, partitionIndex + 1, right);
        }
        return arr;
    }

    private int partition(int[] arr, int left, int right) {
        // 设定基准值（pivot）
        int pivot = left;
        int index = pivot + 1;
        for (int i = index; i <= right; i++) {
            if (arr[i] < arr[pivot]) {
                swap(arr, i, index);
                index++;
            }
        }
        swap(arr, pivot, index - 1);
        return index - 1;
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
```


















### 16.自由落体问题

一球从H米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在 第n次落地时，共经过多少米？会反弹多高？

```java
import java.util.*;
public class Test8 {
    public static void main(String[] args) {
        while(true){
        Scanner read=new Scanner(System.in);
        System.out.println("请输入高度H（米）,次数n");
        int H=read.nextInt(),n=read.nextInt();
        System.out.println("第"+n+"次会反弹"+(double)H/(2<<(n-1))+"米高");  
        fangfa(H,n);
    }}
        public static void fangfa(int H,int n){
            int s=H;
            for(int i=1;i<n;i++){
                H=H/2;
                s+=2*H;
            }
        System.out.println("移动的总路程为："+s);
    }
}
```













### 17.最大公约数和最小公倍数

```java
import java.util.*;
public class Test11{
    public static void main(String[] args) {
        Scanner read =new Scanner(System.in);
        while(read.hasNext()){
        int a=read.nextInt(),b=read.nextInt();
        int  Maxyueshu= gongyinshu(a, b);
        System.out.println("您好：您输入的数字"+a+"和"+b+"的最小公因数为："+Maxyueshu);
        System.out.println("您好：您输入的数字"+a+"和"+b+"的最大公倍数为："+a*b/Maxyueshu);
        }
    }
    public static int gongyinshu(int a,int b) {
        if(a<b) {
            b+=a;
            a=b-a;
            b=b-a;
        }
        while(b!=0) {
            if(a==b)
                return a;
            int x=b;
            b=a%b;
            a=x;
        }
        return a;
    }
}
```










###18. 无重复字符的最长子串
难度中等6268收藏分享切换为英文接收动态反馈
给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
示例 1:
输入: s = "abcabcbb"输出: 3 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
示例 2:
输入: s = "bbbbb"输出: 1解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
示例 3:
输入: s = "pwwkew"输出: 3解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
示例 4:
输入: s = ""输出: 0
提示：
?	0 <= s.length <= 5 * 104
?	s 由英文字母、数字、符号和空格组成

var lengthOfLongestSubstring = function(s) {
    // 哈希集合，记录每个字符是否出现过
    const occ = new Set();
    const n = s.length;
    // 右指针，初始值为 -1，相当于我们在字符串的左边界的左侧，还没有开始移动
    let rk = -1, ans = 0;
    for (let i = 0; i < n; ++i) {
        if (i != 0) {
            // 左指针向右移动一格，移除一个字符
            occ.delete(s.charAt(i - 1));
        }
        while (rk + 1 < n && !occ.has(s.charAt(rk + 1))) {
            // 不断地移动右指针
            occ.add(s.charAt(rk + 1));
            ++rk;
        }
        // 第 i 到 rk 个字符是一个极长的无重复字符子串
        ans = Math.max(ans, rk - i + 1);
    }
    return ans;
};







###19. 整数反转

给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。

如果反转后整数超过 32 位的有符号整数的范围 [?231,  231 ? 1] ，就返回 0。

假设环境不允许存储 64 位整数（有符号或无符号）。
 

示例 1：

输入：x = 123
输出：321
示例 2：

输入：x = -123
输出：-321
示例 3：

输入：x = 120
输出：21
示例 4：

输入：x = 0
输出：0

/**
 * @param {number} x
 * @return {number}
 */

var reverse = function(x) {
    var str = x.toString();
    var newStr;
    if(x >= 0){       
        newStr = "";
        for(var i = str.length - 1; i >= 0 ; i--){
            newStr+=str[i]
        }
        return Number(newStr)
    }else{
        newStr = "-";
        for(var i = str.length - 1; i > 0 ; i--){
            newStr+=str[i]
        }
        return Number(newStr)
    }
    
};








###20. 最长公共前缀
难度简单1816收藏分享切换为英文接收动态反馈
编写一个函数来查找字符串数组中的最长公共前缀。
如果不存在公共前缀，返回空字符串 ""。
 
示例 1：
输入：strs = ["flower","flow","flight"]输出："fl"
示例 2：
输入：strs = ["dog","racecar","car"]输出：""解释：输入不存在公共前缀。
 
提示：
?	1 <= strs.length <= 200
?	0 <= strs[i].length <= 200
?	strs[i] 仅由小写英文字母组成

/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
    if(!strs.length) {
        return '';
    }
    var str = '';

    for(let s of strs[0]) {
        if(!strs.every(c=> c.startsWith(str + s) )) {
            return str;
        }
        str += s;
    }
    return str;
};









###21. 无重复字符的最长子串
难度中等6268收藏分享切换为英文接收动态反馈
给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
示例 1:
输入: s = "abcabcbb"输出: 3 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
示例 2:
输入: s = "bbbbb"输出: 1解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
示例 3:
输入: s = "pwwkew"输出: 3解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
示例 4:
输入: s = ""输出: 0
提示：
?	0 <= s.length <= 5 * 104
?	s 由英文字母、数字、符号和空格组成

var lengthOfLongestSubstring = function(s) {
    // 哈希集合，记录每个字符是否出现过
    const occ = new Set();
    const n = s.length;
    // 右指针，初始值为 -1，相当于我们在字符串的左边界的左侧，还没有开始移动
    let rk = -1, ans = 0;
    for (let i = 0; i < n; ++i) {
        if (i != 0) {
            // 左指针向右移动一格，移除一个字符
            occ.delete(s.charAt(i - 1));
        }
        while (rk + 1 < n && !occ.has(s.charAt(rk + 1))) {
            // 不断地移动右指针
            occ.add(s.charAt(rk + 1));
            ++rk;
        }
        // 第 i 到 rk 个字符是一个极长的无重复字符子串
        ans = Math.max(ans, rk - i + 1);
    }
    return ans;
};














###22.移除元素
难度简单1048收藏分享切换为英文接收动态反馈
给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地?修改输入数组。
元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 
说明:
为什么返回数值是整数，但输出的答案是数组呢?
请注意，输入数组是以「引用」方式传递的，这意味着在函数里修改输入数组对于调用者是可见的。
你可以想象内部操作如下:
// nums 是以“引用”方式传递的。也就是说，不对实参作任何拷贝
int len = removeElement(nums, val);

// 在函数里修改输入数组对于调用者是可见的。
// 根据你的函数返回的长度, 它会打印出数组中 该长度范围内 的所有元素。
for (int i = 0; i < len; i++) {
    print(nums[i]);
}
 
示例 1：
输入：nums = [3,2,2,3], val = 3输出：2, nums = [2,2]解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
示例 2：
输入：nums = [0,1,2,2,3,0,4,2], val = 2输出：5, nums = [0,1,4,0,3]解释：函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。注意这五个元素可为任意顺序。你不需要考虑数组中超出新长度后面的元素。
 
提示：
?	0 <= nums.length <= 100
?	0 <= nums[i] <= 50
?	0 <= val <= 100

var removeElement = function(nums, val) {
    const n = nums.length;
    let left = 0;
    for (let right = 0; right < n; right++) {
        if (nums[right] !== val) {
            nums[left] = nums[right];
            left++;
        }
    }
    return left;
};







23题.递归实现二分法查找









24题.堆排





25题.归并排序




26题.链表反转





27题.两数之和
给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
你可以按任意顺序返回答案。
示例 1：
输入：nums = [2,7,11,15], target = 9输出：[0,1]解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
示例 2：
输入：nums = [3,2,4], target = 6输出：[1,2]
示例 3：
输入：nums = [3,3], target = 6输出：[0,1]
提示：
?	2 <= nums.length <= 104
?	-109 <= nums[i] <= 109
?	-109 <= target <= 109
只会存在一个有效答案

答案：function twoSum(nums, target) {
    let n = nums.length;
    for (let i = 0; i < n; ++i) {
        for (let j = i + 1; j < n; ++j) {
            if (nums[i] + nums[j] == target) {
                return [i, j];
            }
        }
    }
    return [];
}














28题.两数相加

难度中等6909收藏分享切换为英文接收动态反馈

给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
请你将两个数相加，并以相同形式返回一个表示和的链表。
你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
 
示例 1：
 
输入：l1 = [2,4,3], l2 = [5,6,4]输出：[7,0,8]解释：342 + 465 = 807.
示例 2：
输入：l1 = [0], l2 = [0]输出：[0]
示例 3：
输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]输出：[8,9,9,9,0,0,0,1]
 
提示：
?	每个链表中的节点数在范围 [1, 100] 内
?	0 <= Node.val <= 9
?	题目数据保证列表表示的数字不含前导零
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    let f = parseInt(l1.reverse().join(''));
    let s = parseInt(l2.reverse().join(''));
    const result = f + s;
    return result.toString().split('').reverse().map(c=> parseInt(c));
};











30题. 合并两个有序链表
难度简单1965收藏分享切换为英文接收动态反馈
将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
 
示例 1：
 
输入：l1 = [1,2,4], l2 = [1,3,4]输出：[1,1,2,3,4,4]
示例 2：
输入：l1 = [], l2 = []输出：[]
示例 3：
输入：l1 = [], l2 = [0]输出：[0]
 
提示：
?	两个链表的节点数目范围是 [0, 50]
?	-100 <= Node.val <= 100
?	l1 和 l2 均按 非递减顺序 排列

var mergeTwoLists = function(l1, l2) {
    if (l1 === null) {
        return l2;
    } else if (l2 === null) {
        return l1;
    } else if (l1.val < l2.val) {
        l1.next = mergeTwoLists(l1.next, l2);
        return l1;
    } else {
        l2.next = mergeTwoLists(l1, l2.next);
        return l2;
    }
};




